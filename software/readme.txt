pictureStuff.py takes, compresses, and archives a picture
PicSend.py will send all the data from one machine to the other
PicRecv.py gathers data and creates a picture

Run PicRecv.py on a different machine first, then execute runner

Steps to run code:
1. On sending machine, type "chmod +x runner"
2. On receiving machine, type "python3 PicRecv.py"
3. On sending machine, type "./runner"

There should be a few outputs, including how long it will take to send, and how many packets there are. Then it'll output the first line of picture data
PicRecv.py should also output the first line of picture data